angular.module 'linkShortener', [
	'ngAnimate', 
	'ngCookies', 
	'ngTouch', 
	'ngSanitize', 
	'ngMessages', 
	'ngAria', 
	'ngResource', 
	'ui.router', 
	'ngMaterial', 
	'toastr'
]
