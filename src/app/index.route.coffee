angular.module 'linkShortener'
  .config ($stateProvider, $urlRouterProvider, $locationProvider) ->
    'ngInject'

    $locationProvider.html5Mode(true)
    
    $stateProvider
      .state 'home',
        url: '/'
        templateUrl: 'app/home/home.html'
        controller: 'HomeController'
        controllerAs: 'home'

    $urlRouterProvider.otherwise '/'
