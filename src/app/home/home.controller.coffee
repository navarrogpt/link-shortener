angular.module 'linkShortener'
  .controller 'HomeController', ($scope, $window, toastr, ShortenedUrl) ->
    $scope.shortenedUrlObj = {}
    $scope.shortenedUrlResult = null
    $scope.links = []

    filters = {}

    data = ShortenedUrl.query filters, ->
      $scope.links = data

    $scope.create = ->
      if $scope.forms.shortenedUrlForm.$valid
        $scope.buildParams()
        
        ShortenedUrl.save shortened_url: $scope.shortenedUrlObj, ((resp) ->
          $scope.shortenedUrlResult = resp
        ), (error) ->
          toastr.error 'Error shortening url'

      else
        toastr.error 'Please fill in the required fields.'

    $scope.buildParams = ->
      $scope.shortenedUrlObj.url = $scope.shortenedUrl.url

    # get visitor information and open link
    $scope.openLink = (id, url) ->
      resp = JSON.parse $.ajax(
        type: 'GET'
        url: "#{BASE_URL}/api/shortened_urls/#{id}/open_link"
        async: false).responseText

      $window.open(resp.url, '_blank')


    # show link info
    $scope.showLinkInfo = (id) ->
      data = ShortenedUrl.get id: id, ->
        $scope.linkInfo = data




    