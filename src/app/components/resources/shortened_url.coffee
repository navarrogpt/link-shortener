angular.module 'linkShortener'
  .factory 'ShortenedUrl',
    ['$resource'
    ($resource) ->
      ShortenedUrl = $resource "#{BASE_URL}/api/shortened_urls/:id", {id: '@id'},
        {
          query:
            isArray: true
          # create:
          #   method: 'POST'
          #   url: '#{BASE_URL}/api/shortened_urls'
        }

      ShortenedUrl
    ]
